#!/bin/bash -e

node_dirs=()
node_pids=()

expected_pow="${expected_pow:-0.0}"
expected_connections="${expected_connections:-3}"
node_dir="${DATA_DIR-./tzdata}"
peers=("--no-bootstrap-peers")

# we're exposing through docker on 0:8732
# peers+=("--private-mode")

if ! [ -f "${node_dir}/config.json" ]; then
    octez-node config init \
          --network "sandbox" \
          --data-dir "$node_dir" \
          --net-addr ":9731" \
          --rpc-addr ":8732" \
          --expected-pow "$expected_pow" \
          --connections "$expected_connections" \
          --cors-header='content-type' \
          --cors-origin='*'
fi

[ -f "${node_dir}/identity.json" ] || octez-node identity generate "$expected_pow" --data-dir "$node_dir"
octez-node run --synchronisation-threshold 0 --network "sandbox" --rpc-addr 0.0.0.0:8732 --allow-all-rpc 0.0.0.0:8732 --data-dir "$node_dir" "${peers[@]}" --sandbox=sandbox.json "$@" 2>&1 | tee /home/tezos/sandbox.log
