FROM tezos/tezos:master

ENV DATA_DIR /data

USER root
RUN apk update && apk add --no-cache bash vim supervisor
RUN mkdir -p /data /home/tezos && chown -R tezos. /data /home/tezos

WORKDIR /home/tezos
COPY supervisord.conf /etc/
COPY *.sh *.json /home/tezos/
RUN chmod +x /home/tezos/*.sh

USER tezos

EXPOSE 8732
EXPOSE 9731

CMD /usr/bin/supervisord
ENTRYPOINT /usr/bin/supervisord
