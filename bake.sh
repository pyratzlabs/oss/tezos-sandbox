#!/bin/bash -e

declare -A protocols
protocols[edo2]=PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA
protocols[florence]=PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i
protocols[granada]=PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV
protocols[ithaca]=Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A
protocols[genesis]=ProtoGenesisGenesisGenesisGenesisGenesisGenesk612im
protocols[alpha]=ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK
protocols[hangzhou]=PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx
protocols[jakarta]=PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY
protocols[kathmandu]=PtKathmankSpLLDALzWw7CGD2j2MtyveTwboEYokqUCP4a1LxMg
protocols[nairobi]=PtNairobiyssHuh87hEhfVBGCVrK3WnS8Z2FT4ymB5tAa4r1nQf

proto=${protocols[nairobi]}

if [ -n "$PROTO" ]; then
    if [ -n "${protocols[$PROTO]}" ]; then
        proto=${protocols[$PROTO]}
    else
        proto=$PROTO
    fi
fi

if [ ! -f ${DATA_DIR-./tzdata}/.bootstrap.identities ]; then
    BOOTSTRAP1_IDENTITY="tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
    BOOTSTRAP1_PUBLIC="edpkuBknW28nW72KG6RoHtYW7p12T6GKc7nAbwYX5m8Wd9sDVC9yav"
    BOOTSTRAP1_SECRET="unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh"

    BOOTSTRAP2_IDENTITY="tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN"
    BOOTSTRAP2_PUBLIC="edpktzNbDAUjUk697W7gYg2CRuBQjyPxbEg8dLccYYwKSKvkPvjtV9"
    BOOTSTRAP2_SECRET="unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo"

    BOOTSTRAP3_IDENTITY="tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
    BOOTSTRAP3_PUBLIC="edpkuTXkJDGcFd5nh6VvMz8phXxU3Bi7h6hqgywNFi1vZTfQNnS1RV"
    BOOTSTRAP3_SECRET="unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ"

    BOOTSTRAP4_IDENTITY="tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
    BOOTSTRAP4_PUBLIC="edpkuFrRoDSEbJYgxRtLx2ps82UdaYc1WwfS9sE11yhauZt5DgCHbU"
    BOOTSTRAP4_SECRET="unencrypted:edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3"

    BOOTSTRAP5_IDENTITY="tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv"
    BOOTSTRAP5_PUBLIC="edpkv8EUUH68jmo3f7Um5PezmfGrRF24gnfLpH3sVNwJnV5bVCxL2n"
    BOOTSTRAP5_SECRET="unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm"

    ALICE_IDENTITY="tz1Yigc57GHQixFwDEVzj5N1znSCU3aq15td"
    ALICE_PUBLIC="edpkuhJRth9s12SApCJBwgdCpcHbjxqth6HXCZBwAnheSoysqKT23R"
    ALICE_SECRET="unencrypted:edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q"

    BOB_IDENTITY="tz1RTrkJszz7MgNdeEvRLaek8CCrcvhTZTsg"
    BOB_PUBLIC="edpkunkfS48iTAJ1hP7aaEfBkwPT6wPMBjGXBoUZohj8Jupia4wwRm"
    BOB_SECRET="unencrypted:edsk4YDWx5QixxHtEfp5gKuYDd1AZLFqQhmquFgz64mDXghYYzW6T9"

    ACTIVATOR_SECRET="unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6"
    octez-client import secret key bootstrap1 ${BOOTSTRAP1_SECRET}
    octez-client import secret key bootstrap2 ${BOOTSTRAP2_SECRET}
    octez-client import secret key bootstrap3 ${BOOTSTRAP3_SECRET}
    octez-client import secret key bootstrap4 ${BOOTSTRAP4_SECRET}
    octez-client import secret key bootstrap5 ${BOOTSTRAP5_SECRET}
    octez-client import secret key alice ${ALICE_SECRET}
    octez-client import secret key bob ${BOB_SECRET}
    octez-client import secret key activator ${ACTIVATOR_SECRET}
    touch ${DATA_DIR-./tzdata}/.bootstrap.identities
fi

if [ -z "$INIT_TIMESTAMP" ]; then
  INIT_TIMESTAMP="$(TZ='AAA+1' date +%FT%TZ)"
fi

if [ ! -f ${DATA_DIR-./tzdata}/.bootstrap.protocol ]; then
    echo Trying protocol activation, should work if tezos node is ready
    octez-client -E "http://0.0.0.0:8732" -block genesis activate protocol $proto with fitness 1 and key activator and parameters sandbox-parameters.json --timestamp $INIT_TIMESTAMP &> /dev/null || exit 1
    touch ${DATA_DIR-./tzdata}/.bootstrap.protocol
fi

if [ -z "$MANUAL_BAKE" ]; then
    while true; do
        for i in $(seq 1 5); do
            until octez-client -E "http://0.0.0.0:8732" bake for --minimal-timestamp &> /dev/null; do
                sleep 1
            done
            sleep 1
        done
    done
fi
